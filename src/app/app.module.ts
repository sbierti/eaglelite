import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { OrchestratoreComponent } from './orchestratore/orchestratore.component';

import { ConfigAService } from './services/stores/config-a.service';
import { ConfigBService } from './services/stores/config-b.service';
import { ConfigCService } from './services/stores/config-c.service';
import { ConfigJointService } from './services/stores/config-joint.service';
import { ComunicazioneService } from './services/dispatchers/comunicazione.service';

import { MotoreRicercaComponent } from './motore-ricerca/motore-ricerca.component';
import { MappaComponent } from './mappa/mappa.component';

@NgModule({
  declarations: [
    AppComponent,
    OrchestratoreComponent,
    MotoreRicercaComponent,
    MappaComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [ ConfigAService, ConfigBService, ConfigCService, ConfigJointService, ComunicazioneService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
