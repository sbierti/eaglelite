/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import { Http } from '@angular/http';

import { AppComponent } from './app.component';
import { OrchestratoreComponent } from './orchestratore/orchestratore.component';
import { MotoreRicercaComponent } from './motore-ricerca/motore-ricerca.component';

import { ConfigAService } from './services/stores/config-a.service';

describe('AppComponent', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        OrchestratoreComponent,
        MotoreRicercaComponent
      ],
      providers: [ Http, ConfigAService ]
    });
  });

  it('should create the app', async(() => {
    let fixture = TestBed.createComponent(AppComponent);
    let app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it(`should have as title 'app works!'`, async(() => {
    let fixture = TestBed.createComponent(AppComponent);
    let app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('app works!');
  }));

  it('should render title in a h1 tag', async(() => {
    let fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    let compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('app works!');
  }));
});
