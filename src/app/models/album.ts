/**
 * Created by sbierti on 06/12/16.
 */
export class Album {
  userId: number;
  id: number;
  title: string;
}
