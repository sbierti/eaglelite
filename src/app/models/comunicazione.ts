export class Comunicazione {
    sender: string;
    receiver: string;
    msg: string;

    constructor(sender:string,receiver:string,msg:string){
        this.sender = sender;
        this.receiver = receiver;
        this.msg = msg;
    }
}
