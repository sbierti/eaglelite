import {PhotoItem} from "./photo-item";
/**
 * Created by sbierti on 06/12/16.
 */
export class AlbumCompleto {

  userId: number;
  id: number;
  title: string;
  photo: PhotoItem;

  constructor(userId:number,id:number,title:string){
    this.userId = userId;
    this.id = id;
    this.title = title;
  }
}
