import { Component, OnInit } from '@angular/core';
import {ConfigBService} from "../services/stores/config-b.service";
import {PhotoItem} from "../models/photo-item";
import {ConfigCService} from "../services/stores/config-c.service";
import {ComunicazioneService} from "../services/dispatchers/comunicazione.service";
import {Comunicazione} from "../models/comunicazione";

@Component({
  selector: 'app-motore-ricerca',
  templateUrl: './motore-ricerca.component.html',
  styleUrls: ['./motore-ricerca.component.css']
})
export class MotoreRicercaComponent implements OnInit {

  //data: PhotoItem[];
  messaggiRicevuti: string[] = [];

  constructor( public serviceB: ConfigBService, private serviceC: ConfigCService, private comunicazione: ComunicazioneService) { }

  ngOnInit() {
    this.comunicazione.comunicazioniPerMotore.subscribe(messaggio => this.messaggiRicevuti.push(messaggio));
    this.serviceB.getPhotos().subscribe();
  }

  comunica(){
    let comunicazione = new Comunicazione('motore-ricerca','OrchestratoreComponent','ciao dal motore di ricerca');
    this.comunicazione.genericaComunicazione(comunicazione);
  }

}
