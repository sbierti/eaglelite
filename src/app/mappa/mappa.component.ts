import { Component, OnInit } from '@angular/core';
import {ConfigJointService} from "../services/stores/config-joint.service";
import {ConfigCService} from "../services/stores/config-c.service";
import {ComunicazioneService} from "../services/dispatchers/comunicazione.service";
import {AlbumCompleto} from "../models/album-completo";

@Component({
  selector: 'app-mappa',
  templateUrl: './mappa.component.html',
  styleUrls: ['./mappa.component.css']
})
export class MappaComponent implements OnInit {

  data: string;
  messaggio: string = 'mappa dice: "mi hanno interagito"';

  constructor(private configJoint: ConfigJointService, private comunicazione: ComunicazioneService) { }

  ngOnInit() {
    this.configJoint.getJointBC().subscribe(res=>{
      this.data=res
    });
  }

  segnalaInterazione(){
    this.comunicazione.mappaComunica(this.messaggio);
  }

}
