import { Component, OnInit } from '@angular/core';
import { ConfigAService } from '../services/stores/config-a.service';
import {ComunicazioneService} from "../services/dispatchers/comunicazione.service";

@Component({
  selector: 'app-orchestratore',
  templateUrl: './orchestratore.component.html',
  styleUrls: ['./orchestratore.component.css']
})
export class OrchestratoreComponent implements OnInit {

  data: string;
  ultimoMessaggio:string = '';

  constructor(private configurationService: ConfigAService, private comunicazioni: ComunicazioneService) { }

  ngOnInit() {
    this.configurationService.getUsers().subscribe(res => this.data = res);
    this.comunicazioni.comunicazioneGenerica$.filter((comunicazione)=>comunicazione.receiver==='OrchestratoreComponent').subscribe((comunicazione)=>{this.ultimoMessaggio = comunicazione.msg;}
    );
  }

}
