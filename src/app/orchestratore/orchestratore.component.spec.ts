/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { OrchestratoreComponent } from './orchestratore.component';
import { MotoreRicercaComponent } from '../motore-ricerca/motore-ricerca.component';

describe('OrchestratoreComponent', () => {
  let component: OrchestratoreComponent;
  let fixture: ComponentFixture<OrchestratoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrchestratoreComponent, MotoreRicercaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrchestratoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
