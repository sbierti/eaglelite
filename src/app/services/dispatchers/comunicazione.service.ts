import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject} from 'rxjs';
import {Comunicazione} from "../../models/comunicazione";

@Injectable()
export class ComunicazioneService {

    /*
     Streams delle informazioni:
     Subject & BehaviorSubjects implementano le interfacce
     IObserver ( next(), error(), completed() )
     IObservable ( subscribe() )
     Li metto a private per evitare l'accesso all'emissioni dall'esterno

     BehaviorSubject ha la caratteristica di emettere l'ultimo valore precedente
     ogni volta che viene fatto un subscribe().
     Questo è il motivo per il quale bisogna dare un default nel costruttore
     nel nostro caso una stringa vuota

     */

    //comunicazioni peer-to-peer
    private comunicazioniMappa = new BehaviorSubject<string>('');
    private comunicazioniMotore = new BehaviorSubject<string>('');

    // comunicazione broadcast
    private comunicazioneGenerica = new Subject<Comunicazione>();

    // Streams disponibili come public
    comunicazioniPerMotore = this.comunicazioniMappa.asObservable();
    comunicazioniPerMappa = this.comunicazioniMotore.asObservable();
    comunicazioneGenerica$ = this.comunicazioneGenerica.asObservable();

    constructor(){}

    mappaComunica(msg:string){
        this.comunicazioniMappa.next(msg);
    }

    motoreComunica(msg:string){
        this.comunicazioniMotore.next(msg);
    }

    genericaComunicazione(comunicazione:Comunicazione){
        this.comunicazioneGenerica.next(comunicazione);
    }
}
