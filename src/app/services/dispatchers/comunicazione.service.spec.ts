/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ComunicazioneService } from './comunicazione.service';

describe('ComunicazioneService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ComunicazioneService]
    });
  });

  it('should ...', inject([ComunicazioneService], (service: ComunicazioneService) => {
    expect(service).toBeTruthy();
  }));
});
