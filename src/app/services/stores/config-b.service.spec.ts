/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ConfigBService } from './config-b.service';

describe('ConfigBService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ConfigBService]
    });
  });

  it('should ...', inject([ConfigBService], (service: ConfigBService) => {
    expect(service).toBeTruthy();
  }));
});
