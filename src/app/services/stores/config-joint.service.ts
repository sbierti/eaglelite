import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import 'rxjs/add/operator/map';
import {ConfigAService} from "./config-a.service";
import {ConfigBService} from "./config-b.service";
import {ConfigCService} from "./config-c.service";

@Injectable()
export class ConfigJointService {

  constructor(private serviceA: ConfigAService,private serviceB: ConfigBService, private serviceC: ConfigCService) {
  }

  getJointBC(): Observable<any>{
    return Observable.forkJoin(
        this.serviceC.getAlbums(),
        this.serviceB.getPhotos()
    ).map(data=>console.dir(data[0]));
  }

}
