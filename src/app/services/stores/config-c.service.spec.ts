/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ConfigCService } from './config-c.service';

describe('ConfigCService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ConfigCService]
    });
  });

  it('should ...', inject([ConfigCService], (service: ConfigCService) => {
    expect(service).toBeTruthy();
  }));
});
