import { Injectable } from '@angular/core';
import {Http} from "@angular/http";
import {Observable} from "rxjs";

@Injectable()
export class ConfigCService {

  url: string = 'https://jsonplaceholder.typicode.com/albums';

  constructor( private http:Http) { }

  getAlbums(): Observable<any>{
    return this.http.get(this.url).map(res=>res.json());
  }

}
