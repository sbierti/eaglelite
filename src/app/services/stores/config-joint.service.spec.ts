/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ConfigJointService } from './config-joint.service';

describe('ConfigJointService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ConfigJointService]
    });
  });

  it('should ...', inject([ConfigJointService], (service: ConfigJointService) => {
    expect(service).toBeTruthy();
  }));
});
