import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import {PhotoItem} from "../../models/photo-item";

@Injectable()
export class ConfigBService {

  url: string = 'https://jsonplaceholder.typicode.com/photos';

  //photos: Observable<any>;
  photos: Observable<PhotoItem[]>;

  constructor( private http: Http) {
    this.photos = this.http.get(this.url).map((res)=>res.json());
  }

  getPhotos(): Observable<PhotoItem[]>{
    return this.http.get(this.url).map((res)=>res.json());
  }

}
