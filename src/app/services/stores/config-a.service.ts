import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

@Injectable()
export class ConfigAService {

  endpoint: string = 'https://jsonplaceholder.typicode.com/users';

  constructor(private http: Http) { }

  getUsers(): Observable<any> {
    return this.http.get(this.endpoint).map((response)=>response.json());
  }
}
