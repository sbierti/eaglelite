import { EaglelitePage } from './app.po';

describe('eaglelite App', function() {
  let page: EaglelitePage;

  beforeEach(() => {
    page = new EaglelitePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
