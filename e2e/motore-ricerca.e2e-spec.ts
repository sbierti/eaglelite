import { EaglelitePage } from './app.po';
import {element, by} from "protractor";

describe('interazione componente motore di ricerca', function() {
  let page: EaglelitePage;

  beforeEach(()=>{
    page = new EaglelitePage();
  });

  it('visualizza il template del componente',()=>{
      page.navigateTo();
      let motorericerca = element.all(by.tagName('app-motore-ricerca'));
      expect(motorericerca).toBeDefined(true);
  });

  it('visualizza il pulsante per inviare una comunicazione generica',()=>{
      page.navigateTo();
      let motorericerca = element.all(by.tagName('app-motore-ricerca'));
      let pulsante = motorericerca.all(by.id('generatoreComunicazioneGenerica'));
      expect(pulsante).toBeDefined(true);
  });

  it('click sul pulsante per inviare una comunicazione generica',()=>{
    page.navigateTo();
    let motorericerca = element.all(by.tagName('app-motore-ricerca'));
    let pulsante = motorericerca.all(by.id('generatoreComunicazioneGenerica'));
    pulsante.click().then(function () {
      let visualizzatore = element.all(by.tagName('p'));
      expect(visualizzatore.get(0).getText()).toBe('ciao dal motore di ricerca');
    });
  });
});
